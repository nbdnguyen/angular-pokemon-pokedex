import { Component, OnInit } from '@angular/core';
import { PokemonService } from "../../services/pokemon/pokemon.service";
import { ActivatedRoute } from '@angular/router';
import { ProfileService } from "../../services/profile/profile.service";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  name: string = "";
  poke: any;
  inCollection = false;
  loading = true;

  constructor(private pokemon: PokemonService,
              private route: ActivatedRoute,
              private profile: ProfileService) {
  }

  async ngOnInit(): Promise<any> {
    this.name = this.route.snapshot.paramMap.get("id");
    if (this.profile.inCollection(this.name)) {
      this.inCollection = await this.profile.inCollection(this.name);
    }
    await this.getPokemon(this.name);
    this.loading = false;
  }

  public async getPokemon(name) {
    try {
      this.poke = await this.pokemon.getPokemon(name);
    } catch(e) {
      console.error(e);
    }
  }

  collecting() {
    this.inCollection = true;
    this.profile.addToCollection(this.name);
  }

  removing() {
    this.inCollection = false;
    this.profile.removeFromCollection(this.name);
  }

}
