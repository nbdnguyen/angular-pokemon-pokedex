import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/services/profile/profile.service';
import { Router } from '@angular/router';
import { PokemonService } from "src/app/services/pokemon/pokemon.service";

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.scss']
})
export class TrainerComponent implements OnInit {

  collection = [];
  sprites: any = {};
  isEmpty = true;

  constructor(private profile: ProfileService,
              private router: Router,
              private pokemon: PokemonService) { }

  async ngOnInit(): Promise<any> {
    this.collection = await this.profile.getCollection();
    if (this.collection.length == 0) {
      this.isEmpty = true;
    } else {
      this.isEmpty = false;
    }
    await this.getPhoto();
  }

  clickedLogout() {
    this.profile.logOut();
    this.router.navigate([""]);
  }

  public async getPhoto() {
    await this.collection.forEach(async poke => {
      let curPoke;
      try {
        curPoke = await this.pokemon.getPokemon(poke);
      } catch(e) {
        console.error(e);
      }
      this.sprites[poke] = curPoke.sprites.front_default;
    })
  }
  
  async removing(poke) {
    await this.profile.removeFromCollection(poke);
    try {
      this.collection = await this.profile.getCollection();
    } catch(e) {
      console.error(e);
    }
    if (this.collection.length == 0) {
      this.isEmpty = true;
    }
  }

  clickedPoke(event, name) {
    this.router.navigate(["pokemon", name]);
  }

}
