import { Component, OnInit } from '@angular/core';
import { ProfileService } from "../../services/profile/profile.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  name = "";
  constructor(private profile: ProfileService) { }

  ngOnInit(): void {
  }
  
  loggedIn() {
    this.name = this.profile.getTrainer("trainer").replace(/\"/g, "");
    return this.profile.isLoggedIn();
  }

}
