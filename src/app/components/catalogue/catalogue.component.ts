import { Component, OnInit } from '@angular/core';
import { PokemonService } from "../../services/pokemon/pokemon.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss']
})
export class CatalogueComponent implements OnInit {

  data: any = [];
  sprites: any = {};
  loading: boolean = true;

  constructor(private pokemon: PokemonService,
              private router: Router) {}

  async ngOnInit() {
    await this.getList("https://pokeapi.co/api/v2/pokemon?limit=893");
    //await this.getList("https://pokeapi.co/api/v2/pokemon?limit=3");
    this.getPhoto();
  }
  
  public async getList(offset) {
    try {
      this.data = await this.pokemon.getList(offset);
    } catch(e) {
      console.error(e);
    }
  }

  public async getPhoto() {
    this.loading = true; 
    await this.data.results.forEach(async poke => {
      let detail;
      try {
        detail = await this.pokemon.getList(poke.url);
        this.loading = false;
      } catch(e) {
        console.error(e);
      }
      this.sprites[poke.name] = detail.sprites.front_default;
    })
  }

  clickedPoke(event, name) {
    console.log(name);
    this.router.navigate(["pokemon", name]); //redirect
  }
}
