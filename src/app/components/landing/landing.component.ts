import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from "../../services/profile/profile.service";
import { FormGroup, FormControl, Validators, AbstractControl }
  from "@angular/forms";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  public trainerForm: FormGroup = new FormGroup({
    trainername: new FormControl("",
      [Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20)
    ])
  });

  get trainername(): AbstractControl {
    return this.trainerForm.get("trainername");
  }

  constructor(private profile: ProfileService,
              private router: Router) {}

  ngOnInit(): void {
    if (this.profile.isLoggedIn()) {
      this.router.navigate(["pokemon"]);
    }
  }
  
  onTrainernameClicked(event: any) {
    this.profile.logIn("trainer", this.trainername.value);
    this.router.navigate(["pokemon"]); //redirect
  }


}
