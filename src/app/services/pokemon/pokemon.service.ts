import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  poke: any = [];

  constructor(private http: HttpClient) {}
  
  getPokemon(poke: string): Promise<any> {
    const url = "https://pokeapi.co/api/v2/pokemon/" + poke;
    return this.http.get(url).toPromise();
  }

  getList(offset: string): Promise<any> {
    return this.http.get(offset).toPromise();
  }
}
