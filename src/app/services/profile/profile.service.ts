import { Injectable, Testability } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { setStorage, getStorage } from "../../utils/storage";

@Injectable({
  providedIn: 'root'
})

export class ProfileService {
  
  name: "";
  loggedIn: boolean;
  
  constructor(private http: HttpClient) {}

  public async addToCollection(pokemon) {
    let collection = [];
    try {
      collection = await getStorage("collection");
    } catch(e) {
      console.error(e);
    }
    if (!(await this.inCollection(pokemon))) {
      collection.push(pokemon);
    }
    setStorage("collection", collection);
  }
  public async removeFromCollection(pokemon) {
    let collection = [];
    try {
      collection = await getStorage("collection");
    } catch(e) {
      console.error(e);
    }
    const index = collection.indexOf(pokemon);
    if (index > -1) {
      collection.splice(index,1);
    }
    setStorage("collection", collection);
  }
  public async inCollection(pokemon) {
    let collection = [];
    try {
      collection = await getStorage("collection");
    } catch(e) {
      console.error(e);
    }
    if (collection.indexOf(pokemon) > -1) {
      return true;
    } else {
      return false;
    }
  }

  public async getCollection() {
    let collection = [];
    try {
      collection = await getStorage("collection");
    } catch(e) {
      console.error(e);
    }
    return collection;
  }

  public addTrainer(key, trainer) {
    setStorage(key, JSON.stringify(trainer));
  }
  public getTrainer(key) {
    return getStorage(key);
    
  }
  public isLoggedIn(): boolean {
    const stringBool = JSON.stringify(getStorage("loggedin"))
    return (stringBool === '"true"');
  }
  public addLoginBool() {
    setStorage("loggedin", JSON.stringify(this.loggedIn));
  }
  public async logIn(key, trainer) {
    let prevTrainer = {};
    try {
      prevTrainer = await this.getTrainer("trainer");
    } catch(e) {
      console.error(e);
    } 
    if (!(prevTrainer == '"' + trainer + '"')) {
      this.addTrainer(key, trainer);
      setStorage("collection", []);
    }
    this.name = trainer;
    this.loggedIn = true;
    this.addLoginBool();
  }
  public logOut() {
    this.loggedIn = false;
    this.addLoginBool();
  }
}
