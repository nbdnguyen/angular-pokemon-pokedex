import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './components/landing/landing.component';
import { DetailComponent } from './components/detail/detail.component';
import { TrainerComponent } from './components/trainer/trainer.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { AuthGuard } from "./guards/auth/auth.guard"

const routes: Routes = [
  {
    path: "landing",
    component: LandingComponent
  },
  {
    path: "trainer",
    component: TrainerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pokemon",
    component: CatalogueComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "pokemon/:id",
    component: DetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "",
    pathMatch: "full",
    redirectTo: "landing"
  },
  {
    path: '**',
    pathMatch: "full",
    redirectTo: "landing"
    //if logged in, send to trainer, if not send to landing.
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
