## Pokémon Pokédex

TODO LIST:

* header

* collection and collection button

* css

An app that works as the Pokédex.

Contains:

* **Landing page**: Trainer inputs their name and page redirects them to the Trainer page.

* **Trainer page**: A list of all the currently collected Pokémon with their avatar. Each item is clickable and redirects to the corresponding Pokémons Detail page.

* **Pokémon Catalogue page**: Inaccessible if a trainer name has yet to be registered. A carded list of all the available Pokémon, each item is clickable and redirects to the corresponding Pokémons Detail page.

* **Pokémon Detail page**: A collect button that saves the Pokémon to the Trainer page in the collected Pokémon list. Information sorted as:

    * **Base Stats**: Image, types, base stats, name.

    * **Profile**: Height, weight, abilities, base experience.

    * **Movies**: A list of moves.



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
